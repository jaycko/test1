var express = require("express");

var app= express();
app.use("/public",express.static('public'));

app.get("/",function(req,res){
  res.sendFile(__dirname + "/views/index.html")
})
app.get("/data",function(req,res){
  res.json({ message: 'dato json enviado desde el servidor' }); 
})
app.listen(8080);
